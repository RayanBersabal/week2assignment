class Node {
    constructor(element) {
      this.element = element;
      this.next = null;
    }
  }
  
  class LinkedList {
    constructor() {
      this.head = null;
      this.size = 0;
    }
  
    add(element) {
     
      var node = new Node(element);
  
      var current;
  
      if (this.head == null) this.head = node;
      else {
        current = this.head;
  
  
        while (current.next) {
          current = current.next;
        }
  
        current.next = node;
      }
      this.size++;
    }
  
    insertAt(element, index) {
      if (index < 0 || index > this.size)
        return console.log("Please enter a valid index");
      else {
        var node = new Node(element);
        var current, prev;
  
        current = this.head;
  
        if (index == 0) {
          node.next = this.head;
          this.head = node;
        } else {
          current = this.head;
          var it = 0;
  
          while (it < index) {
            it++;
            prev = current;
            current = current.next;
          }
  
          node.next = current;
          prev.next = node;
        }
        this.size++;
      }
    }
  
    removeFrom(index) {
      if (index < 0 || index >= this.size)
        return console.log("Please Enter a valid index");
      else {
        var current,
          prev,
          it = 0;
        current = this.head;
        prev = current;
  
        if (index === 0) this.head = current.next;
        else {
          
          while (it < index) {
            it++;
            prev = current;
            current = current.next;
          }
  
          prev.next = current.next;
        }
        this.size--;
  
        return current.element;
      }
    }
  
    removeElement(element) {
      var current = this.head;
      var prev = null;
  
      
      while (current != null) {
        
        if (current.element === element) {
          if (prev == null) this.head = current.next;
          else {
            prev.next = current.next;
          }
          this.size--;
          return current.element;
        }
        prev = current;
        current = current.next;
      }
      return -1;
    }
  
    indexOf(element) {
      var count = 0;
      var current = this.head;
  
      
      while (current != null) {
        
        if (current.element === element) return count++;
        count++;
        current = current.next;
      }
  
      return -1;
    }
  
    isNotEmpty() {
      return this.size > 0;
    }
  

    listSize() {
      return this.size;
    }
  

    listPrint() {
    var current = this.head;
    var str = "";
    while (current) {
        str += current.element + " ";
        current = current.next;
    }
      console.log(str);
    }
  }
  


var ll = new LinkedList();

console.log(ll.isNotEmpty()); 

ll.add("a");

ll.listPrint();

console.log(ll.listSize());

ll.add("b");
ll.add("c");
ll.add("d");
ll.add("e");
  

ll.listPrint();
  

console.log("removed by search element :" + ll.removeElement("d"));
  
ll.listPrint();
  

console.log("Index of 'c' is " + ll.indexOf("c"));
  
ll.insertAt("z", 2);

ll.listPrint();

console.log("is List Not Empty : " + ll.isNotEmpty());
  
console.log("removed by index element : " + ll.removeFrom(3));
  
ll.listPrint();
